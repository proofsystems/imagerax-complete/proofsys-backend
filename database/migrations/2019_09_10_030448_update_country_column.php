<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCountryColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('country')->unsigned()->nullable()->change();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('country','country_id');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_country_id_foreign');
            $table->dropIndex('users_country_id_foreign');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('country_id','country');
            $table->string('country')->nullable()->change();
        });
    }
}
