<?php

namespace App;

use App\Notifications\PasswordResetNotification;
use Application\Traits\Imaging;
use Domain\Country\Country;
use Domain\Follower\Follower;
use Domain\LinkedSocialAccount\LinkedSocialAccount;
use Domain\Posts\Post;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,SoftDeletes, Notifiable;
    use Imaging;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname','lname', 'email', 'password','image','country_id','cover_photo','description','wallet_address','wallet_public_key','wallet'
    ];

    protected $image_fields = ['image','cover_photo'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'image_address',
        'social_account',
        'is_followed',
        'num_post',
    ];

    protected $with =['country:id,country_name'];
    protected $withCount =['followers','following'];

    public static function boot()
    {
      parent::boot();

      static::creating(function($model){
        static::storeImage($model);
      });

      static::updating(function($model){
        static::updateImage($model);
      });

      static::deleting(function ($model) {
        static::deleteImage($model);
      });
    }


  public function getImageAddressAttribute(){
    return asset('/storage/' . $this->image);
  }

  public function getFullNameAttribute(){
    return $this->fname.' '.$this->lname;
  }

  public function country() {
    return $this->belongsTo(Country::class,'country_id');
  }

  /**
   * Description: Followers
   * Date: November 03, 2019 (Sunday)
   **/
  public function followers() {
    return $this->hasMany(Follower::class,'following_user_id','id');
  }

  /**
   * Description: Followers
   * Date: November 03, 2019 (Sunday)
   **/
  public function following() {
    return $this->hasMany(Follower::class,'user_id','id');
  }

  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new PasswordResetNotification($token));
  }

  public function linkedSocialAccounts()
  {
    return $this->hasOne(LinkedSocialAccount::class);
  }

  public function getSocialAccountAttribute() {
    return $this->linkedSocialAccounts;
  }

 /**
   * Description: isFollowed
   * Date: November 15, 2019 (Friday)
   **/
  public function getIsFollowedAttribute() {
    return Follower::where('user_id',auth('api')->user()->id)
      ->where('following_user_id',$this->id)->count();
  }

  /**
   * Description: Number of Post
   * Date: November 19, 2019 (Tuesday)
   **/
  public function getNumPostAttribute() {
    return Post::where('user_id',$this->id)->count();
  }
}
