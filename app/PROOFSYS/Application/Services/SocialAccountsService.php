<?php
namespace Application\Services;

use App\User;
use Domain\LinkedSocialAccount\LinkedSocialAccount;
use Laravel\Socialite\Two\User as ProviderUser;

class SocialAccountsService
{
    /**
     * Find or create user instance by provider user instance and provider name.
     * 
     * @param ProviderUser $providerUser
     * @param string $provider
     * 
     * @return User
     */
    public function findOrCreate(ProviderUser $providerUser, string $provider): User
    {
        $linkedSocialAccount = LinkedSocialAccount::where('provider_name', $provider)
            ->where('provider_id', $providerUser->getId())
            ->first();
        if ($linkedSocialAccount) {
            return $linkedSocialAccount->user;
        } else {
            $user = null;
            if ($email = $providerUser->getEmail()) {
                $user = User::where('email', $email)->first();
            }
            if (! $user) {
                if ($provider == 'google') {
                    $fname = $providerUser->user['given_name'];
                    $lname = $providerUser->user['family_name'];
                } else if ($provider = 'FACEBOOK' || $provider = 'facebook') {
                    $fname = $providerUser->user['first_name'];
                    $lname = $providerUser->user['last_name'];
                }
                $user = User::create([
                    'fname' => $fname,
                    'lname' => $lname,
                    'email' => $providerUser->getEmail()
                ]);
            }
            $user->linkedSocialAccounts()->create([
                'provider_id' => $providerUser->getId(),
                'provider_name' => $provider,
            ]);
            return $user;
        }
    }
}