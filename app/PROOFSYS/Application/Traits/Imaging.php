<?php

/*
 * This file is part of Imaging.
 *
 * (c) Gether Kestrel B. Medel <gether.medel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
|--------------------------------------------------------------------------
| Auto Upload, Delete and Resizing of Images
|--------------------------------------------------------------------------
|
|
*/

namespace Application\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;

trait Imaging
{
    /**
     * Get storage path for images, photos or pictures.
     *
     * @return string
     */
    public static function storagePath()
    {
        return 'images';
    }

    /**
     * Get compression rate.
     *
     * @return int
     */
    public static function compressionRate()
    {
        return 70;
    }

    /**
     * Check whether to auto resize the image, photo or picture.
     *
     * @return true
     */
    public static function auteResize()
    {
        return true;
    }

    /**
     * Default resolution.
     *
     * @return string
     */
    public static function defaultResolution()
    {
        return '766';
    }

    /**
     * default 16:9 ratio resolutions.
     *
     * @return array
     */
    public static function resolutions()
    {
        return [
            '766' => [
                'width'  => 1366,
                'height' => 766
            ],
            '900' => [
                'width'  => 1600,
                'height' => 900
            ],
            '1080' => [
                'width'  => 1920,
                'height' => 1080
            ],
            '1440' => [
                'width'  => 2560,
                'height' => 1440
            ],
            '2160' => [
                'width'  => 3840,
                'height' => 2160
            ]
        ];
    }

    /**
     * If directory doesn't exist create directory.
     *
     * @return void
     */
    public static function createDirectory()
    {
        Storage::makeDirectory('public/' . self::storagePath(), 0775, true);
    }


    public static function createImageDirectory()
    {
        Storage::makeDirectory('public/images/' . self::storagePath(), 0775, true);
    }

    public static function createImageFieldDirectory($module,$field)
    {
        Storage::makeDirectory('public/images/'.$module.'/'.$field, 0775, true);
    }

    /**
     * Check if file exists.
     *
     * @param  string $imageName Image file name.
     * @return boolean
     */
    public static function checkIfImageExist($imageName)
    {
        $storagePath = self::storagePath();
        return Storage::exists('public/' . $storagePath . '/' . $imageName);
    }

    /**
     * Create sizes and store image, photo or picture automatically.
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @param  array $sizes
     * @return void
     */
    public static function storeImage($model)
    {
        $video_format = [
            '','mp4', 'm4a', 'm4v', 'f4v', 'f4a', 'm4b', 'm4r', 'f4b', 'mov', '3gp', '3gp2', '3g2', '3gpp', '3gpp2', 'ogg', 'oga', 'ogv', 'ogx', 'wmv', 'wma', 'flv', 'avi', 'MP4', 'M4A', 'M4V', 'F4V', 'F4A', 'M4B', 'M4R', 'F4B', 'MOV', '3GP', '3GP2', '3G2', '3GPP', '3GPP2', 'OGG', 'OGA', 'OGV', 'OGX', 'WMV', 'WMA', 'FLV', 'AVI', 'gif', 'GIF'
        ];

        $image_format = [
            '','tiff', 'jpg', 'jpeg','png', 'TIFF', 'JPG', 'JPEG', 'PNG'
        ];

        foreach($model->image_fields as $image_field){
          if(request()->hasFile($image_field)){
            self::createImageDirectory();
            self::createImageFieldDirectory($model->table,$image_field);
            $file = request()->file($image_field);
            $time = Carbon::now()->format('YmdTHis');
            $name = $time.rand(111, 999) . '.' . $file->getClientOriginalExtension();
            $path = 'images/'.$model->table.'/'.$image_field;
            $storage_path = 'app/public/'.$path;
            $model->{$image_field} = 'storage/'.$path.'/'.$name;

            $file_ext = $file->getClientOriginalExtension();

            if (array_search($file_ext,$image_format)>0) {
                $model->type = 'image';
            };

            if (array_search($file_ext,$video_format)>0) {
                $model->type = 'video';
            }
            $file->move(storage_path($storage_path), $name);
          }
        }


    }

    /**
     * Delete image including sizes.
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public static function deleteImage($model)
    {
          foreach($model->image_fields as $image_field){
              Storage::delete('public/'.str_replace('storage/', '', $model->{$image_field}));
          }
    }

    /**
     * Update image including sizes.
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public static function updateImage($model)
    {
      self::deleteImage($model);
      self::storeImage($model);
    }
}
