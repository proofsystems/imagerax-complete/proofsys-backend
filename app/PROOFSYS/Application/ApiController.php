<?php

namespace Application;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class ApiController extends Controller
{
  public function __construct($repository, $module){
    // $this->middleware('jwt.auth');
    $this->repository = $repository;
    $this->module = $module;
  }

  public function index(Request $request){
    return response()->json($this->repository->paginateWithFilters($request->all()),200);
  }

  public function all(Request $request){
    return response()->json($this->repository->all($request->all()),200);
  }

  public function store(Request $request){
    $repository = $this->repository->create($request->all());
    return response()->json([
      'message' => $this->module.' Successfully Created',
      'data' => $repository->toArray()
    ],200);
  }


  public function update(Request $request,$id){
    $repository = $this->repository->update($request->all(),$id);
    return response()->json([
      'message' => $this->module.' Successfully Updated'
    ],200);
  }

  public function destroy($id){
    $this->repository->delete($id);
    return response()->json([
      'message' => $this->module.' Successfully Deleted'
    ],200);
  }

  public function show($id){
    return response()->json($this->repository->find($id)->toArray(),200);
  }

  public function relation($id,$relation){
    return response()->json($this->repository->relation($id,$relation)->toArray(),200);
  }

  public function getByCategory($slug){
    return response()->json($this->repository->getByCategory($slug)->toArray(),200);

  }



}
