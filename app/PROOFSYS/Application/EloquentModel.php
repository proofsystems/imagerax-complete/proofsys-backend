<?php

namespace Application;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
class EloquentModel extends Model
{
		use SoftDeletes;
    use SoftCascadeTrait;

    protected $hidden = ['created_at','updated_at','deleted_at'];

		public function scopeWithHas($query, $relation, $constraint){
    	return $query->whereHas($relation, $constraint)
                 ->with([$relation => $constraint]);
		}

		public function getImageAddressAttribute(){

      if($this->image && Storage::exists('/public/' .$this->image)){
        return asset('/storage/' . $this->image);
      }

      return asset('/placeholders/content.png');

    }

}
