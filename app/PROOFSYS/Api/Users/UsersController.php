<?php

namespace Api\Users;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\Follower\FollowerRepository;
use Domain\Notification\NotificationRepository;
use Domain\User\UserRepository;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends ApiController
{
  public function __construct(UserRepository $user,NotificationRepository $notification, FollowerRepository $follower){
    parent::__construct($user,'User');
    $this->user = $user;
    $this->notification = $notification;
    $this->follower = $follower;
  }
  /**
   * Get the guard to be used during authentication.
   *
   * @return \Illuminate\Contracts\Auth\Guard
   */
  public function guard()
  {
      return Auth::guard('api');
  }

  /**
   * Description: Update User
   * Date: September 27, 2019 (Friday)
   **/
  public function updateUser(Request $request) {
    $repository = $this->user->updateUser($request->all());
    return response()->json([
      'message' => $this->module.' Successfully Updated'
    ],200);
  }


  public function updatePassword(Request $request) {
    $validator = Validator::make($request->all(), [
      'old_password' => 'required',
      'password' => 'required|min:6',
      'confirm_password' => 'required_with:password|same:password|min:6'
    ]);

    $validator->after(function ($validator) use ($request) {
        if (!Hash::check($request->old_password,$this->guard()->user()->password)) {
            $validator->errors()->add('old_password', "Old password doesn't match");
        }
    });

    if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first()],406);
    }

    $user = $this->guard()->user();
    $user->password = Hash::make($request->password);
    $user->save();
    return response()->json(['message'=>'Password successfully updated.']);

  }

  /**
   * Description: User's notification
   * Date: September 25, 2019 (Wednesday)
   **/
  public function notifications() {
    return $this->notification->own();
  }

  /**
   * Description: User's notification
   * Date: September 25, 2019 (Wednesday)
   **/
  public function all_notifications() {
    return $this->notification->all();
  }

  /**
   * Description: Followers
   * Date: October 30, 2019 (Wednesday)
   **/
  public function followers() {
    return $this->follower->followers();
  }

  /**
   * Description: Following
   * Date: October 30, 2019 (Wednesday)
   **/
  public function following() {
    return $this->follower->following();
  }

  /**
   * Description: Suggestions
   * Date: November 12, 2019 (Tuesday)
   **/
  public function suggestions() {
    return $this->user->suggestions();
  }

  /**
   * Description: User Search
   * Date: November 12, 2019 (Tuesday)
   **/
  public function search(Request $request) {
    return $this->user->search($request);
  }

  /**
   * Description: Users Following
   * Date: November 13, 2019 (Wednesday)
   **/
  public function user_following($id) {
    return $this->follower->user_following($id);
  }

  /**
   * Description: Users Following
   * Date: November 13, 2019 (Wednesday)
   **/
  public function user_followers($id) {
    return $this->follower->user_followers($id);
  }
}
