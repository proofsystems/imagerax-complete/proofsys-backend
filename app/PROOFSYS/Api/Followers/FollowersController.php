<?php

namespace Api\Followers;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\Follower\FollowerRepository;
use Domain\Notification\NotificationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FollowersController extends ApiController
{
  public function __construct(FollowerRepository $follower, NotificationRepository $notify){
    parent::__construct($follower,'Follow');
    $this->follower = $follower;
    $this->notify = $notify;
  }

  /**
   * Description: 
   * Date: 
   **/
  public function follow($user_id) {
    return $this->follower->follow($user_id);
  }
}
