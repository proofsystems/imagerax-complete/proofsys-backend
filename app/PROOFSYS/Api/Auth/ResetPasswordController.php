<?php

namespace Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    use ResetsPasswords;

    /**
     * Description: Send Reset Response
     * Date: August 29, 2019 (Thursday)
     **/
    protected function sendResetResponse(Request $request, $response) {
      return response(['message'=>trans($response)]);
    }

    /**
     * Description: Send Reset Failed Response
     * Date: August 29, 2019 (Thursday)
     **/
    protected function sendResetFailedResponse(Request $request, $response) {
      return response(['error'=>trans($response)],422 );
    }
}
