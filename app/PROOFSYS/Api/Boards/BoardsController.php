<?php

namespace Api\Boards;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\Board\BoardRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BoardsController extends ApiController
{
  public function __construct(BoardRepository $board){
    parent::__construct($board,'Board');
    $this->board = $board;
  }

  /**
   * Description: Owned Board
   * Date: December 03, 2019 (Tuesday)
   **/
  public function owned() {
    return $this->board->owned();
  }
}
