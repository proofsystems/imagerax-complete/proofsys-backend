<?php
Route::middleware(['auth:api'])->group(function () {

  $controller = '\Api\Boards\BoardsController';

  Route::group(['prefix' => 'boards'],function() use ($controller){
    Route::group(['middleware'=>'api'],function() use ($controller){
      Route::get('all',$controller.'@all');
      Route::get('owned',$controller.'@owned');
    });
  });

  Route::resource('boards', $controller)->middleware('auth:api');
});
