<?php
Route::middleware(['auth:api'])->group(function () {

  $controller = '\Api\Comments\CommentsController';

  Route::group(['prefix' => 'comments'],function() use ($controller){
    Route::group(['middleware'=>'api'],function() use ($controller){
      Route::get('all',$controller.'@all');
      Route::get('like/{comment_id}',$controller.'@likeComment');
    });
  });

  Route::resource('comments', $controller)->middleware('auth:api');
});
