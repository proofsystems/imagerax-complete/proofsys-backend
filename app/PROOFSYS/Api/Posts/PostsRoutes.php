<?php
Route::middleware(['auth:api'])->group(function () {

  $controller = '\Api\Posts\PostsController';

  Route::group(['prefix' => 'posts'],function() use ($controller){
    Route::group(['prefix' => 'profile'],function() use ($controller){
    Route::get('user/{user_id}',$controller.'@user_profile_feeds');
    Route::get('own',$controller.'@user_profile_own_feeds');
    });
    Route::get('all',$controller.'@all');
    Route::get('own',$controller.'@own');
    Route::get('like/{post_id}',$controller.'@likePost');

    Route::group(['prefix' => 'favorites'],function() use ($controller){
      Route::get('today',$controller.'@today_favorites');
      Route::get('week',$controller.'@week_favorites');
      Route::get('month',$controller.'@month_favorites');
      Route::get('/',$controller.'@favorites');
    });

    Route::group(['prefix' => 'trends'],function() use ($controller){
      Route::get('today',$controller.'@today_trends');
      Route::get('week',$controller.'@week_trends');
      Route::get('month',$controller.'@month_trends');
      Route::get('/',$controller.'@trends');
    });

    Route::get('feeds',$controller.'@feeds');

    Route::post('pin',$controller.'@pin');

  });

  Route::resource('posts', $controller)->middleware('auth:api');
});
