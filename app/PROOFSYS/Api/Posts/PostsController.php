<?php

namespace Api\Posts;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\Like\LikeRepository;
use Domain\Notification\NotificationRepository;
use Domain\PostBoard\PostBoardRepository;
use Domain\Posts\PostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PostsController extends ApiController
{
  public function __construct(PostBoardRepository $postboard, PostRepository $post, NotificationRepository $notification, LikeRepository $like){
    parent::__construct($post,'Post');
    $this->post = $post;
    $this->notification = $notification;
    $this->like = $like;
    $this->postboard = $postboard;
  }

  public function store(Request $request){
    $post =  $request->all();
    $post['user_id']=Auth('api')->user()->id;
    DB::beginTransaction();
    try {
      $post = $this->post->create($post);
      $this->postboard->create([
        'post_id'=>$post->id,
        'board_id'=>$request->board_id
      ]);
      DB::commit();
      $post['user']=Auth('api')->user();
      $this->notification->sendNotification($post,'POST','New image has been uploaded.');
    } catch (Exception $e) {
      DB::rollBack();
      return $e;
    }

    return response()->json([
      'message' => 'Post Successfully Created',
      'data' => $post->toArray()
    ],200);
  }

  /**
   * Description: User's Own Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function own() {
    return $this->post->own();
  }

  /**
   * Description: Like Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function likePost($post_id) {
    return $this->like->like($post_id,'post');
  }

  /**
   * Description: All Trends
   * Date: September 24, 2019 (Tuesday)
   **/
  public function trends() {
    return $this->post->trends();
  }

  /**
   * Description: Trend POst
   * Date: September 24, 2019 (Tuesday)
   **/
  public function today_trends() {
    return $this->post->today_trends();
  }

  /**
   * Description: Trend POst for this week
   * Date: September 24, 2019 (Tuesday)
   **/
  public function week_trends() {
    return $this->post->week_trends();
  }

  /**
   * Description: Trend Post for this month
   * Date: September 24, 2019 (Tuesday)
   **/
  public function month_trends() {
    return $this->post->month_trends();
  }

  /**
   * Description: User's Favorites
   * Date: September 24, 2019 (Tuesday)
   **/
  public function favorites() {
    return $this->post->favorites();
  }

  /**
   * Description: User's favorites for today
   * Date: September 25, 2019 (Wednesday)
   **/
  public function today_favorites() {
    return $this->post->today_favorites();
  }

  /**
   * Description: User's favorites for this week
   * Date: September 25, 2019 (Wednesday)
   **/
  public function week_favorites() {
    return $this->post->week_favorites();
  }

  /**
   * Description: User's favorites for this month
   * Date: September 25, 2019 (Wednesday)
   **/
  public function month_favorites() {
    return $this->post->month_favorites();
  }

  /**
   * Description: Users Feed
   * Date: November 03, 2019 (Sunday)
   **/
  public function feeds() {
    return $this->post->feeds();
  }

  /**
   * Description: Pin Post
   * Date: December 03, 2019 (Tuesday)
   **/
  public function pin(Request $request) {
    return $this->post->pin($request);
  }

  /**
   * Description: User's Profile Feeds
   * Date: December 04, 2019 (Wednesday)
   **/
  public function user_profile_feeds($id) {
    return $this->post->user_profile_feeds($id);
  }

  /**
   * Description: User's Profile Feeds
   * Date: December 04, 2019 (Wednesday)
   **/
  public function user_profile_own_feeds() {
    return $this->post->user_profile_own_feeds();
  }
}
