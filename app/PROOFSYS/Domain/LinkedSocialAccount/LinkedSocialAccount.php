<?php

namespace Domain\LinkedSocialAccount;

use App\User;
use Application\EloquentModel;
class LinkedSocialAccount extends EloquentModel
{
  protected $fillable = [
    'provider_name',
    'provider_id',
  ];
  
  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
