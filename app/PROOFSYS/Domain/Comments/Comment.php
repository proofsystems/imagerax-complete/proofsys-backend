<?php

namespace Domain\Comments;

use App\User;
use Application\EloquentModel;
use Domain\Like\Like;
use Domain\Notification\Notification;
use Domain\Posts\Post;

class Comment extends EloquentModel
{
    protected $table = 'comments';

    protected $fillable =
    [
        'user_id',
        'post_id',
        'comment'
    ];

    protected $with = ['user','likes'];
    protected $withCount = ['likes'];

    protected $cascadeDeleteMorph = ['likes', 'notifications'];

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notifiable');
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->notifications()->delete();
            $model->likes()->delete();
        });
    }
}


