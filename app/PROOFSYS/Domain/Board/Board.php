<?php

namespace Domain\Board;

use App\User;
use Application\EloquentModel;
use Domain\PostBoard\PostBoard;
use Domain\Posts\Post;

class Board extends EloquentModel
{

    protected $table = 'boards';

    protected $fillable =
    [
        'name',
        'user_id',
        'visibility'
    ];

    protected $with = ['owned_by','posts'];

    /**
     * Description: Posts on Board
     * Date: December 02, 2019 (Monday)
     **/
    public function post_board() {
      return $this->hasMany(PostBoard::class,'board_id','id');
    }

    /**
     * Description: Posts on Board
     * Date: December 03, 2019 (Tuesday)
     **/
    public function posts() {
      return $this->hasManyThrough(
        Post::class,
        PostBoard::class,
        'board_id',
        'id',
        'id',
        'post_id'
      );
    }

    /**
     * Description: Board Owner
     * Date: December 03, 2019 (Tuesday)
     **/
    public function owned_by() {
      return $this->belongsTo(User::class,'user_id');
    }

    /**
     * Description: Posts
     * Date: December 02, 2019 (Monday)
     **/
    public function getPostsAttribute() {
      $items = $this->post_board()->selectRaw('post_id,board_id')->with('post')->where('board_id',$this->id)->get();
      if ($items->count()>0) {
        $i=0;
        foreach ($items as $item) {
          $posts[$i++]=$item->post;
          }
        return $posts;
      }
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->user_id = auth('api')->user()->id;
        });

        static::updating(function ($model) {
        });

        static::deleting(function ($model) {
          $model->post_board()->delete();
          $check = $model->posts()->where('posts.user_id',auth('api')->user()->id)->forceDelete();
        });
    }
}



