<?php

namespace Domain\Board;

use Application\Repository;
use Domain\Board\Board;
use Illuminate\Http\Request;

class BoardRepository extends Repository
{

  protected $board;

  public function __construct(Board $board){
    parent::__construct($board);
    $this->board = $board;
  }

  /**
   * Description: Owned
   * Date: December 03, 2019 (Tuesday)
   **/
  public function owned() {
    return $this->board->where('user_id',$this->user()->id)->get();
  }
}
