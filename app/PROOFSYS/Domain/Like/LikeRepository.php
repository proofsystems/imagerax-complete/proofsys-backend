<?php

namespace Domain\Like;

use Application\Repository;
use Domain\Like\Like;
use Domain\Notification\NotificationRepository;

class LikeRepository extends Repository
{
  protected $like, $notify;

  public function __construct(Like $like, NotificationRepository $notify)
  {
      parent::__construct($this->like = $like);
      $this->notify = $notify;
  }

  /**
   * Description: Like Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function like($id,$type)
  {
    ($type=='post')?$likeable_type='Domain\Posts\Post':$likeable_type='Domain\Comments\Comment';
    $check = $this->like->where('likeable_id',$id)
      ->where('likeable_type',$likeable_type)
      ->where('user_id',auth()->user()->id)
      ->count();
      if ($check == 0) {
        $data = [
          'user_id'=>auth()->user()->id,
          'likeable_id'=>$id,
          'likeable_type'=>$likeable_type
        ];
        $data = $this->like->create($data);
        $this->notify->notify($type,'like',$id);
        return response()->json(['message'=>'Like']);
      } else {
        $this->like->where('likeable_id',$id)
        ->where('likeable_type',$likeable_type)
        ->where('user_id',auth()->user()->id)
        ->forceDelete();
        return response()->json(['message'=>'Unlike']);
      }
  }
}
