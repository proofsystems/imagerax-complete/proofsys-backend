<?php

namespace Domain\Like;

use App\User;
use Application\EloquentModel;

class Like extends EloquentModel
{
  protected $table = 'user_likes';

  protected $fillable =
  [
    'user_id',
    'likeable_id',
    'likeable_type',
  ];

  protected $with =['user'];

  public function likeable()
  {
    return $this->morphTo();
  }

  public function user()
  {
    return $this->belongsTo(User::class,'user_id');
  }
}
