<?php

namespace Domain\PostBoard;

use Application\Repository;
use Domain\PostBoard\PostBoard;
use Illuminate\Http\Request;

class PostBoardRepository extends Repository
{

  protected $postboard;

  public function __construct(PostBoard $postboard){
    parent::__construct($postboard);
  }
}
