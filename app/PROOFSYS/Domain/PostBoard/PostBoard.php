<?php

namespace Domain\PostBoard;

use App\User;
use Application\EloquentModel;
use Domain\Board\Board;
use Domain\Posts\Post;

class PostBoard extends EloquentModel
{

  protected $table = 'post_boards';

  protected $fillable =
  [
    'user_id',
    'board_id',
    'post_id'
  ];

  /**
   * Description: Post Relationship
   * Date: December 02, 2019 (Monday)
   **/
  public function post() {
    return $this->belongsTo(Post::class,'post_id');
  }

  /**
   * Description: Board Relationship
   * Date: December 02, 2019 (Monday)
   **/
  public function board() {
    return $this->belongsTo(Board::class,'board_id');
  }

  /**
   * Description: User Relationship
   * Date: December 02, 2019 (Monday)
   **/
  public function user() {
    return $this->belongsTo(User::class,'user_id');
  }

  public static function boot()
  {
    parent::boot();

    static::creating(function ($model) {
      $model->user_id = auth('api')->user()->id;
    });

    static::updating(function ($model) {
    });

    static::deleting(function ($model) {
      $model->post()->where('user_id',auth('api')->user()-id)->forceDelete();
    });
  }
}



