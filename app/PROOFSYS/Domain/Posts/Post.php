<?php

namespace Domain\Posts;

use App\User;
use Application\EloquentModel;
use Application\Traits\Imaging;
use Domain\Comments\Comment;
use Domain\Like\Like;
use Domain\Notification\Notification;
use Domain\PostBoard\PostBoard;

class Post extends EloquentModel
{
    use Imaging;

    protected $table = 'posts';

    protected $fillable =
    [
        'user_id',
        'caption',
        'image',
        'type'
    ];

    protected $image_fields = ['image'];

    // protected $softCascade = [];

    protected $cascadeDeleteMorph = ['likes', 'notifications'];

    protected $with = ['comments','likes','post_by'];

    protected $withCount = ['likes'];

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notifiable');
    }

    /**
     * Description: Board
     * Date: December 02, 2019 (Monday)
     **/
    public function board() {
      return $this->hasOne(PostBoard::class,'post_id','id');
    }

    /**
     * Description: Return User
     * Date: October 14, 2019 (Monday)
     **/
    public function post_by() {
      return $this->belongsTo(User::class,'user_id');
    }


    /**
     * Description: Board
     * Date: December 02, 2019 (Monday)
     **/
    public function getPostBoardAttribute() {
      return $this->board()->where('user_id',$this->user_id)->first()->board;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            static::storeImage($model);
        });

        static::updating(function ($model) {
            static::updateImage($model);
        });

        static::deleting(function ($model) {
            static::deleteImage($model);
            $model->notifications()->forceDelete();
            $model->likes()->forceDelete();
            $model->board()->forceDelete();
            $model->comments()->forceDelete();
        });
    }
}



