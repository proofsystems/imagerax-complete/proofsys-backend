<?php

namespace Domain\Posts;

use Application\Repository;
use Carbon\Carbon;
use Domain\Follower\Follower;
use Domain\Like\Like;
use Domain\PostBoard\PostBoard;
use Domain\Posts\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostRepository extends Repository
{

  protected $post;

  public function __construct(PostBoard $postboard, Post $post, Like $like){
    parent::__construct($post);
    $this->post = $post;
    $this->like = $like;
    $this->postboard = $postboard;
  }

  /**
   * Description: All Modified
   * Date: September 27, 2019 (Friday)
   **/
  public function all()
  {
    $items = $this->post->orderBy('created_at','DESC')->get();
    foreach ($items as $key => $item) {
      $check = $item->likes->where('user_id',auth('api')->user()->id)->count();
      if ($check>0) {
        $item['isLiked']=1;
      } else {
        $item['isLiked']=0;
      }
    }
    return $items;
  }

  /**
   * Description: User's Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function own() {
    return $this->post->where('user_id',auth()->user()->id)->orderBy('created_at','DESC')->get();
  }

  /**
   * Description: Trends
   * Date: September 24, 2019 (Tuesday)
   **/
  public function today_trends() {
    return $this->post
     ->where(DB::raw('date(created_at)'),date('Y-m-d'))
     ->having('likes_count','>',0)
     ->orderBy('likes_count','DESC')
     ->take(10)
     ->get();
  }

  /**
   * Description: All Trends
   * Date: September 24, 2019 (Tuesday)
   **/
  public function trends() {
    return $this->post
     ->having('likes_count','>',0)
     ->orderBy('likes_count','DESC')
     ->get();
  }

  /**
   * Description: Trend POst for this week
   * Date: September 24, 2019 (Tuesday)
   **/
  public function week_trends() {
    $now = Carbon::now();
    $weekStartDate = $now->startOfWeek()->format('Y-m-d');
    $weekEndDate = $now->endOfWeek()->format('Y-m-d');
    
    return $this->post
     ->whereBetween(DB::raw('date(created_at)'),[$weekStartDate,$weekEndDate])
     ->having('likes_count','>',0)
     ->orderBy('likes_count','DESC')
     ->take(10)
     ->get();
  }

  /**
   * Description: Trend Post for this month
   * Date: September 24, 2019 (Tuesday)
   **/
  public function month_trends() {
    $now = Carbon::now();
    $monthStartDate = $now->startOfMonth()->format('Y-m-d');
    $monthEndDate = $now->endOfMonth()->format('Y-m-d');
    
    return $this->post
     ->whereBetween(DB::raw('date(created_at)'),[$monthStartDate,$monthEndDate])
     ->having('likes_count','>',0)
     ->orderBy('likes_count','DESC')
     ->take(10)
     ->get();
  }

  /**
   * Description: User's Favorites
   * Date: September 24, 2019 (Tuesday)
   **/
  public function favorites() {
    return $this->post->whereHas('likes',function($q){
      $q->where('user_id',auth()->user()->id);
    })->get();
  }

  /**
   * Description: User's favorites for today
   * Date: September 25, 2019 (Wednesday)
   **/
  public function today_favorites() {
    return $this->post->whereHas('likes',function($q){
      $q->where('user_id',auth()->user()->id)
        ->where(DB::raw('date(created_at)'),date('Y-m-d'));
    })->get();
  }

  /**
   * Description: User's favorites for this week
   * Date: September 25, 2019 (Wednesday)
   **/
  public function week_favorites() {
    $now = Carbon::now();
    $weekStartDate = $now->startOfWeek()->format('Y-m-d');
    $weekEndDate = $now->endOfWeek()->format('Y-m-d');

    return $this->post->whereHas('likes',function($q) use ($weekStartDate,$weekEndDate){
      $q->where('user_id',auth()->user()->id)
        ->whereBetween(DB::raw('date(created_at)'),[$weekStartDate,$weekEndDate]);
    })->get();
  }

  /**
   * Description: User's favorites for this month
   * Date: September 25, 2019 (Wednesday)
   **/
  public function month_favorites() {
    $now = Carbon::now();
    $monthStartDate = $now->startOfMonth()->format('Y-m-d');
    $monthEndDate = $now->endOfMonth()->format('Y-m-d');

    return $this->post->whereHas('likes',function($q) use ($monthStartDate,$monthEndDate){
      $q->where('user_id',auth()->user()->id)
        ->whereBetween(DB::raw('date(created_at)'),[$monthStartDate,$monthEndDate]);
    })->get();
  }

  /**
   * Description: User's Feed
   * Date: November 03, 2019 (Sunday)
   **/
  public function feeds() {
    $ffs = Follower::where('user_id',auth('api')->user()->id)->get();

    $post = $this->post->orderBy('created_at','DESC')
    ->orWhere('user_id',auth('api')->user()->id);

    foreach ($ffs as $ff) {
      $post = $post->orWhere('user_id',$ff->following->id);
    }
    $post = $post->get();
    return $post;
  }

  /**
   * Description: Pin POst
   * Date: December 03, 2019 (Tuesday)
   **/
  public function pin($request) {
    $check = $this->postboard
      ->where('post_id',$request->post_id)
      ->where('board_id',$request->board_id)
      ->where('user_id',$this->user()->id)
      ->first();
    if (is_null($check)) {
      $this->postboard->create($request->all());
      return response()->json(['message'=>'Pinned'],200);
    }
  }

  /**
   * Description: User's Profile Feeds
   * Date: December 04, 2019 (Wednesday)
   **/
  public function user_profile_feeds($id) {
    return $this->post
      ->where('user_id',$id)
      ->orWhereHas('board',function($q) use ($id){
        $q->where('user_id',$id);
      })
      ->get()->makeHidden(['comments','likes']);
  }

  /**
   * Description: User's Own Profile Feeds
   * Date: December 04, 2019 (Wednesday)
   **/
  public function user_profile_own_feeds() {
    return $this->post
      ->where('user_id',$this->user()->id)
      ->orWhereHas('board',function($q){
        $q->where('user_id',$this->user()->id);
      })
      ->get()->makeHidden(['comments','likes']);
  }
}
