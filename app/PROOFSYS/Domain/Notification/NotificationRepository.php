<?php

namespace Domain\Notification;

use Domain\Comments\Comment;
use Domain\Notification\Notification;
use Domain\Posts\Post;
use GuzzleHttp\Client;


class NotificationRepository
{
  protected $like;
  protected $notification;

  public function __construct(Notification $notification)
  {
      $this->notification = $notification;
  }

  /**
   * Description: User's Notification top 10
   * Date: September 25, 2019 (Wednesday)
   **/
  public function own() {
    return $this->notification->where('user_id',auth('api')->user()->id)->take(10)
      ->orderBy('created_at','DESC')
      ->get();
  }

  /**
   * Description: User's Notification all
   * Date: September 25, 2019 (Wednesday)
   **/
  public function all() {
    return $this->notification->where('user_id',auth('api')->user()->id)
      ->orderBy('created_at','DESC')
      ->get();
  }

  /**
   * Description: Send Notification
   * Date: September 18, 2019 (Wednesday)
   **/
  public function sendNotification($detail,$type,$msg) {
    $client = new Client();
    $client->request('POST', 'https://onesignal.com/api/v1/notifications', [
      'json' => [
        'app_id' => env('ONESIGNAL_APP_ID','3215f35a-4476-4d52-a270-f165066b36f1'),
        'included_segments' => ['Subscribed Users'],
        'headings' => [
          'en' =>  $msg
        ],
        'contents' => [
          'en' => $detail->caption
        ],
        "ios_badgeType" => "Increase",
        "ios_badgeCount" => 1,
        "large_icon" => env('APP_URL') . '/images/notif-logo.png',
        "data" => [
          "page" => $type,
          "id" => $detail->id,
          "user" => $detail->user->fname.' '.$detail->user->lname,
        ]
      ],
      'headers' => [
        'https'         => 'https://onesignal.com/api/v1/notifications',
        'Authorization' => 'Basic YjJhMGU5ZDktY2QyYy00ZWE2LWFjMzQtMDFmNzljOWE1YWJk',
        'Content-Type'  => 'application/json'

      ]
    ]);
    return $client;
  }

  /**
   * Description: Notification per action
   * Date: September 25, 2019 (Wednesday)
   **/
  public function notify($post_type, $action,$id) {
    $actions = [
      'comment' => 'commented on your',
      'like' => 'liked your',
    ];

    $message =  auth()->user()->fname.' '.auth()->user()->lname.' '.$actions[$action].' '.$post_type;
    if($post_type=='post') {
      $post_type='Domain\Posts\Post';
      $item = Post::find($id);
    } else {
      $post_type='Domain\Comments\Comment';
      $item = Comment::find($id);
    }

    if ($item->user_id == auth('api')->user()->id) {
      return;
    }

    $check = $this->notification->where('notifiable_id',$id)
      ->where('notifiable_type',$post_type)
      ->where('user_id',$item->user_id)
      ->where('sender_user_id',auth()->user()->id)
      ->where('action',$action)
      ->count();
    $data = [
      'item'=>$item,
      'user_id'=>$item->user_id,
      'notifiable_id'=>$id,
      'notifiable_type'=>$post_type,
      'sender_user_id'=>auth()->user()->id,
      'message'=>$message,
      'action'=>$action,
    ];
    if ($check==0) {
      $data = $this->notification->create($data);
    }
    return $data;
  }

  /**
   * Description: Notify Following
   * Date: November 03, 2019 (Sunday)
   **/
  public function notify_follow($user_id,$following_id) {
    if ($user_id == auth('api')->user()->id) {
      return;
    }

    $message =  auth()->user()->fname.' '.auth()->user()->lname.' followed you.';
    $type='Domain\Follower\Follower';
    $action='follow';

    $check = $this->notification->where('notifiable_id',$following_id)
      ->where('notifiable_type',$type)
      ->where('user_id',$user_id)
      ->where('sender_user_id',auth()->user()->id)
      ->where('action',$action)
      ->count();

    $data = [
      'user_id'=>$user_id,
      'notifiable_id'=>$following_id,
      'notifiable_type'=>$type,
      'sender_user_id'=>auth()->user()->id,
      'message'=>$message,
      'action'=>$action,
    ];
    if ($check==0) {
      $data = $this->notification->create($data);
    }
    return $data;
  }
}
