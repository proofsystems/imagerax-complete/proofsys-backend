<?php

namespace Domain\Follower;

use App\User;
use Application\Repository;
use Domain\Follower\Follower;
use Domain\Notification\NotificationRepository;

class FollowerRepository extends Repository
{
  protected $follower, $notify;

  public function __construct(Follower $follower, NotificationRepository $notify, User $user)
  {
      parent::__construct($this->follower = $follower);
      $this->follower = $follower;
      $this->notify = $notify;
      $this->user = $user;
  }

  /**
   * Description: Like Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function follow($user_id)
  {
    // return $user_id;
    if (auth('api')->user()->id == $user_id) {
      return response()->json(['message'=>"You can't follow yourself."],406);
    }
    $check = $this->follower
      ->where('following_user_id',$user_id)
      ->where('user_id',auth('api')->user()->id)
      ->count();
    if ($check == 0) {
      $item = $this->follower->create(['following_user_id'=>$user_id]);
      $this->notify->notify_follow($user_id,$item->id);
      return response()->json(['message'=>"Followed!"],200);
    } else {
      $ap = $this->follower
        ->where('following_user_id',$user_id)
        ->where('user_id',auth('api')->user()->id)
        ->first();
      $ap->notifications()->forceDelete();
      $ap->forceDelete();
      return response()->json(['message'=>"Unfollowed!"],200);
    }
  }

  /**
   * Description: Followers
   * Date: October 30, 2019 (Wednesday)
   **/
  public function followers() {
    $followers = $this->follower->where('following_user_id',auth('api')->user()->id)->get();
    if ($followers->count() > 0) {
      $users = $this->user->orderBy('lname');

      foreach ($followers as $ff) {
        $users = $users->orWhere('id',$ff->following->id);
      }
      $users = $users->get();
      return $users;
    }
  }

  /**
   * Description: Following
   * Date: October 30, 2019 (Wednesday)
   **/
  public function following() {
    $following = $this->follower->where('user_id',auth('api')->user()->id)->get();

    if ($following->count() > 0) {
      $users = $this->user->orderBy('lname');

      foreach ($following as $ff) {
        $users = $users->orWhere('id',$ff->following->id);
      }
      $users = $users->get();
      return $users;
    }
  }

  /**
   * Description: Users Following
   * Date: November 15, 2019 (Friday)
   **/
  public function user_following($id) {
    $following = $this->follower->where('user_id',$id)->get();

    if ($following->count() > 0) {
      $users = $this->user->orderBy('lname');

      foreach ($following as $ff) {
        $users = $users->orWhere('id',$ff->following->id);
      }
      $users = $users->get();
      return $users;
    }
  }

  /**
   * Description: Users Followers
   * Date: November 15, 2019 (Friday)
   **/
  public function user_followers($id) {
    $followers = $this->follower->where('following_user_id',$id)->get();
    if ($followers->count() > 0) {
      $users = $this->user->orderBy('lname');

      foreach ($followers as $ff) {
        $users = $users->orWhere('id',$ff->following->id);
      }
      $users = $users->get();
      return $users;
    }
  }
}
